document.addEventListener( 'DOMContentLoaded', function() { 
    const domBtnMenu = document.getElementById('btn-menu');
    const domToolBar = document.getElementById('toolbar');

    domBtnMenu.addEventListener( 'click', function() {
        
       let isMenuHidden = domToolBar.classList.contains( 'hidden' );
      
        if( isMenuHidden ) {
       
            domToolBar.classList.remove( 'hidden' );
            domBtnMenu.classList.add( 'close' );
        }
        else {
           
            domToolBar.classList.add( 'hidden' );
            domBtnMenu.classList.remove( 'close' );
        }
    });
});