document.addEventListener( 'DOMContentLoaded', function() { 
    const domCmdMainMenu = document.getElementById('toggle-address');

        domCmdMainMenu.addEventListener( 'click', function() {
        var copyText = ("Hadrien Legrais\n" + document.getElementById('address').textContent);
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        alert("Copied the text: " + copyText.value);
    });
});