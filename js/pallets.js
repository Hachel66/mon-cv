document.addEventListener( 'DOMContentLoaded', function() { 
    const domBtnDark = document.getElementById('dark-theme');
    const domBtnLight = document.getElementById('light-theme');
    const domBtnPrint = document.getElementById('print-theme');
    const domHTML = document.getElementById('root');

    domBtnDark.addEventListener( 'click', function() {
        domHTML.classList.add( 'dark' );
        domHTML.classList.remove( 'print' );
    });

    domBtnLight.addEventListener( 'click', function() {
        domHTML.classList.remove( 'dark' );
        domHTML.classList.remove( 'print' );
    });

    domBtnPrint.addEventListener( 'click', function() {
        domHTML.classList.add( 'print' );
        domHTML.classList.remove( 'dark' );
    });

});